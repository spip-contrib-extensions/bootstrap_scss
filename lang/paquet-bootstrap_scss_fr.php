<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'bootstrap_scss_description' => 'Ce plugin permet d\'utiliser et de personnaliser le framework css Bootstrap (via le backoffice) grâce au plugin SCSS pour SPIP',
	'bootstrap_scss_nom' => 'Bootstrap',
	'bootstrap_scss_slogan' => 'Améliorez vos squelettes avec le framework CSS Bootstrap',
);
