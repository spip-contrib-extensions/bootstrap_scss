<?php

/**
 * Fonctions du plugin Bootstrap SCSS
 *
 * @plugin     Bootstrap SCSS
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 * @package    SPIP\bootstrap_scss\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function bootstrap_scss_theme_toggler($type = 'primary', $class = '') {
	if (lire_config('/meta_bootstrap_scss/themes/enable-dark-mode', 'true') == 'false') return;

	return recuperer_fond('balise/theme_bs_toggler', ['type' => $type, 'class' => $class]);
}
