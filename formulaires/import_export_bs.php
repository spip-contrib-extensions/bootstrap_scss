<?php

function formulaires_import_export_bs_saisies_dist() {
	$saisies = [
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'bs_json',
				'label' => 'Votre fichier',
				'nb_fichiers' => 1,
				'obligatoire' => 'oui',
				'explication' => 'Choisissez un fichier au format JSON'
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => [
					'mime' => 'specifique',
					'mime_specifique' => ['application/json']
				]
			]
		]
	];

	return $saisies;
}

function formulaires_import_export_bs_verifier_dist() {
	$erreurs = [];
	$fichier = $_FILES['bs_json']['tmp_name'][0];

	if (!$fichier) return;

	$liste_param = [
		'style_polices',
		'style_liens',
		'style_composants',
		'style_bordures',
		'style_input_btn',
		'fonctionnalites',
		'themes',
		'apparence_globale',
		'couleurs_principales',
		'composants_actifs'
	];

	foreach (_COMPOSANTS as $composant_id => $composant_label) {
		$liste_param[] = 'composant_' . $composant_id;
	}

	$contenu_fichier = file_get_contents($fichier);
	$json_array = json_decode($contenu_fichier, true);

	foreach ($json_array as $key => $value) {
		if (!in_array($key, $liste_param)) {
			$erreurs['bs_json'] = 'Fichier JSON non conforme';
			break;
		}
	}

	return $erreurs;
}

function formulaires_import_export_bs_traiter_dist() {
	$fichier = _request('_fichiers');
	$chemin = $fichier['bs_json'][0]['tmp_name'];
	$contenu_fichier = file_get_contents($chemin);
	$json_array = json_decode($contenu_fichier, true);

	foreach ($json_array as $key => $value) {
		if (!ecrire_config('/meta_bootstrap_scss/' . $key, serialize($value))) {
			$ret['bs_json'] = _T('erreur_technique_enregistrement_impossible');
			return $ret;
		}
	}

	$ret['message_ok'] = _T('config_info_enregistree');

	return $ret;
}

function formulaires_import_export_bs_fichiers_dist() {
	return ['bs_json'];
}
