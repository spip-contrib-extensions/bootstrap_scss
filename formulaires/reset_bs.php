<?php

function formulaires_reset_bs_saisies_dist() {
	$saisies = [
		[
			'saisie' => 'checkbox',
			'options' => [
				'nom' => 'confirm',
				'label' => 'Êtes-vous sûr ?',
				'data' => [
					'oui' => 'oui'
				]
			]
		]
	];

	return $saisies;
}

function formulaires_reset_bs_traiter_dist() {
	$confirm = _request('confirm');
	if (!$confirm) {
		$ret['message_erreur'] = 'Veuillez confirmer votre choix';
		return $ret;
	}

	$ret['message_ok'] = _T('config_info_enregistree');
	$config = lire_config('/meta_bootstrap_scss', []);

	foreach ($config as $key => $value) {
		if (!effacer_config('/meta_bootstrap_scss/' . $key, [])) {
			unset($ret['message_ok']);
			$ret['message_erreur'] = _T('erreur_technique_enregistrement_impossible');
			break;
		}
	}

	return $ret;
}
