<?php

function formulaires_configurer_bs_composants_charger_dist() {
	$valeurs = [];
	$valeurs['composants_actifs'] = lire_config('/meta_bootstrap_scss/composants_actifs', []);

	return $valeurs;
}

function formulaires_configurer_bs_composants_traiter_dist() {
	$ret = [];
	$composants = _request('composants_actifs') ? _request('composants_actifs') : [];

	if (ecrire_config('/meta_bootstrap_scss/composants_actifs', $composants)) {
		$ret['message_ok'] = _T('config_info_enregistree');
	} else {
		$ret['message_erreur'] = _T('erreur_technique_enregistrement_impossible');
	}

	return $ret;
}

function formulaires_configurer_bs_composants_saisies_dist() {
	$saisies = [];
	$saisies[] = [
		'saisie' => 'checkbox',
		'options' => [
			'nom' => 'composants_actifs',
			'label' => 'Liste des composants',
			'data' => _COMPOSANTS
		]
	];

	return $saisies;
}
