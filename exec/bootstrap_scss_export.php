<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function exec_bootstrap_scss_export_dist() {
	$config = lire_config('/meta_bootstrap_scss', []);
	$config_array = [];

	foreach ($config as $key => $value) {
		if ($key != 'charset') {
			$config_array[$key] = unserialize($value);
		}
	}

	$config_json = json_encode($config_array, JSON_PRETTY_PRINT);

	$filename = 'bootstrap_scss_' . date('Y-m-d');

	header('Content-type: application/json');
	header("Content-Type: application/force-download");
	header("Content-Type: application/download");
	header("Content-disposition: " . $filename . ".json");
	header("Content-disposition: filename=" . $filename . ".json");

	echo $config_json;
}
