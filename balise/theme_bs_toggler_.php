<?php

function balise_THEME_BS_TOGGLER__dist($p) {
	preg_match(",^{class=(.*)?}$,", $p->fonctions[0][1], $regs);
	$class = $regs[1] ? $regs[1] : '';

	preg_match(",^THEME_BS_TOGGLER_(.*)?$,", $p->nom_champ, $regs);
	$type = strtolower($regs[1]);

	$p->code = 'bootstrap_scss_theme_toggler("' . $type . '", "' . $class . '")';
	return $p;
}
