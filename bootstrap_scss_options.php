<?php

/**
 * Options globales chargées à chaque hit
 *
 * @plugin     Bootstrap SCSS
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 * @package    SPIP\bootstrap_scss\Options
 */

if (!defined('_ECRIRE_INC_VERSION'))
	return;

$GLOBALS['spip_wheels']['raccourcis'][] = 'bootstrap_scss';


// Les clés sont liées aux noms de fichiers SCSS de la lib Bootstrap
define('_COMPOSANTS', [
	'accordion' => 'Accordéons',
	'alert' => 'Alertes (nécessite Boutons de fermeture)',
	'badge' => 'Badges',
	'navbar' => 'Barres de navigation',
	'progress' => 'Barres de progression',
	'collapse' => 'Blocs dépliables (nécessite Boutons et Cards)',
	'buttons' => 'Boutons',
	'close' => ' Boutons de fermeture',
	'card' => ' Cards',
	'carousel' => 'Carousels',
	'modal' => ' Fenêtres modales',
	'breadcrumb' => ' Fils d\'ariane',
	'forms' => 'Formulaires',
	'button-group' => 'Groupes de boutons',
	'tooltip' => 'Info-bulles',
	'list-group' => 'Listes groupées',
	'dropdown' => 'Menus déroulants',
	'nav' => 'Navigations',
	'offcanvas' => 'Offcanvas',
	'pagination' => 'Pagination',
	'popover' => 'Popovers',
	'tables' => 'Tableaux',
]);
