<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin Bootstrap SCSS
 *
 * @plugin     Bootstrap SCSS
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 * @package    SPIP\bootstrap_scss\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function bootstrap_scss_upgrade($nom_meta_base_version, $version_cible)
{

	$maj = [];
	// Création de la table qui stockera la config du plugin
	$maj['create'][] = array('installer_table_meta', 'meta_bootstrap_scss');

	# $maj['1.1.0']  = array(array('sql_alter','TABLE spip_xx RENAME TO spip_yy'));

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function bootstrap_scss_vider_tables($nom_meta_base_version)
{
	// Suppression de la table qui stocke la config du plugin
	sql_drop_table('spip_meta_bootstrap_scss');

	// Suppression de la librairie Bootstrap
	$dossier_bootstrap = 'lib/bootstrap532';
	if ($folder = find_in_path($dossier_bootstrap)) {
		deleteDirectory($folder);
	}

	// Effacement de la version du schema du plugin dans la table spip_meta
	effacer_meta($nom_meta_base_version);
}


// https://stackoverflow.com/questions/1653771/how-do-i-remove-a-directory-that-is-not-empty
function deleteDirectory($dir)
{
	if (!file_exists($dir)) {
		return true;
	}

	if (!is_dir($dir)) {
		return unlink($dir);
	}

	foreach (scandir($dir) as $item) {
		if ($item == '.' || $item == '..') {
			continue;
		}

		if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
			return false;
		}
	}

	return rmdir($dir);
}
