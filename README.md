# Plugin Bootstrap pour SPIP

## Pourquoi un nouveau plugin autour de Bootstrap ?

Ce plugin, qui dépend du [plugin SCSS](https://git.spip.net/spip-contrib-extensions/scssphp), permet au webmestre de personnaliser le framework directement par le back-office de SPIP (polices, couleurs, composants, etc.).
Cela facilite la mise en place d'une charte graphique (design system) tout en limitant la taille des CSS finaux. De plus, cela évite d'alourdir les CSS avec des surcharges.

## Fonctionnalités du plugin

- Télécharge et installe la version 5.3.2 de Bootstrap dans le dossier `lib` de SPIP lors de l'activation du plugin (et le supprime lors de la désinstallation). Les montées de version de Bootstrap pourront être prises en compte lors des montées de version du plugin.
- Offre un menu de configuration du framework dans le menu `Squelettes` du back-office avec différents onglets :

![Onglets](/captures/onglets.png "Onglets")

- Ces paramètres sont pris en compte lors de la compilation SCSS vers CSS
- Les CSS sont calculées lors du calcul des squelettes grâce au plugin SCSS pour SPIP
- Les CSS caclulées et le JS de Bootstrap sont insérés grâce aux balises `#INSERT_HEAD_CSS` et `#INSERT_HEAD`
- Ajoute des raccourcis dans le porte-plume pour permettre aux publicateurs/publicatrices d'insérer des composants Bootstrap quand ils sont activés

![Porte-Plume](/captures/porte-plume.png "Onglets")

- Permet d'importer, d'exporter et de réinitialiser la configuration du plugin :

![Porte-Plume](/captures/import-export.png "Onglets")

- Offre la balise dynamique `#THEME_BS_TOGGLER_{type}` utilisable dans les squelettes pour ajouter un sélecteur de thème (darkmod, light, auto). Le type sera utilisé pour générer la class `.btn-{type}` sur le sélecteur. Il peut correspondre aux différentes couleurs de Bootstrap (primary, secondary, warning, danger, success, info, light, dark) mais peut être n'importe quelle chaîne si vous souhaitez le styliser vous-même. Vous pouvez ajouter des class CSS. Par ex : `#THEME_BS_TOGGLER_PRIMARY{class=bottom-0 end-0 mb-3 me-3}` Et vous pouvez même surcharger les fichiers `balise/theme_bs_toggler.html` et `js/theme_bs_toggler.js` dans votre dossier `squelettes` pour une personnalisation complète.

## Demo

?page=demo/test_bootstrap_scss

## TODO
- Sur les input de type texte, rajouter la valeur par défaut sur l'attribut explication et l'attribut defaut en plus du placeholder
- Voir si c'est possible d'offrir la possibilité de créer [un thème personnalisé](https://getbootstrap.com/docs/5.3/customize/color-modes/)
