<?php
if (!defined('_ECRIRE_INC_VERSION')) {
  return;
}

function replace_collapse($t) {
  return remplacer_tags($t);
}

function replace_accordion($t) {
  return remplacer_tags($t);
}

function replace_listgroup($t) {
  return remplacer_tags($t);
}

function replace_alert($t) {
  return remplacer_tags($t);
}

function replace_badge($t) {
  return remplacer_tags($t);
}

function replace_progress($t) {
  return remplacer_tags($t);
}

function replace_carousel($t) {
  return remplacer_tags($t);
}

function replace_nav($t) {
  return remplacer_tags($t);
}

function remplacer_tags($input) {
  static $id;
  static $id_item;
  $i = $j = 0;

  // var_dump($input);


  $pattern = '/<(\w+)(?: style=["\'](\w+)["\'])?(?: height=["\']([^"\']+)["\'])?>(.*?)<\/\1>/s';

  $result = preg_replace_callback($pattern, function ($matches) use (&$id, &$id_item, &$i, &$j) {
    static $style;
    static $slide = 0;
    $tag_name = $matches[1];
    $style = $matches[2] ?  $matches[2] : $style;
    $height = $matches[3];
    $content = $matches[4];

    if (in_array($tag_name, ['nav_bs_menu', 'collapse_bs'])) {
      $id = uniqid('bs_');
    }

    switch ($tag_name) {
        // Collapses (Blocs dépliants)
      case 'collapse_bs':
        return '<div class="mb-5">' . remplacer_tags($content) . '</div>';
        break;
      case 'collapse_bs_btn':
        return '<button class="btn btn-primary mb-2" type="button" data-bs-toggle="collapse"  data-bs-target="#' . $id . '" aria-expanded="false" aria-controls="' . $id . '">' . $content . '</button>';
        break;
      case 'collapse_bs_content':
        return '<div id="' . $id . '" class="collapse"><div class="card card-body">' . remplacer_tags($content) . '</div></div>';
        break;
        // List-groups
      case 'listgroup_bs':
        return '<ul class="list-group">' . remplacer_tags($content) . '</ul>';
        break;
      case 'listgroup_bs_item':
        return '<li class="list-group-item' . ($style ? ' list-group-item-' . $style : '') . '">' . remplacer_tags($content) . '</li>';
        break;
        // Carousels
      case 'carousel_bs':
        $id = uniqid('bs_');
        return '<div id="' . $id . '" class="carousel slide mb-5">' .
          '<div class="carousel-inner">' .
          remplacer_tags($content) .
          '</div>' .
          ' <button class="carousel-control-prev" type="button" data-bs-target="#' . $id . '" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#' . $id . '" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>' .
          '</div>';
        break;
      case 'carousel_bs_item':
        $slide++;
        $class_active = $slide == 1 ? ' active' : '';
        return '<div class="carousel-item' . $class_active . '">' . $content . '</div>';
        break;
        // Accordions
      case 'accordion_bs':
        $id = uniqid('bs_');
        return '<div id="' . $id . '" class="accordion mb-5">' . remplacer_tags($content) . '</div>';
        break;
      case 'accordion_bs_item':
        $id_item = uniqid('bs_');
        return '<div class="accordion-item">' . remplacer_tags($content) . '</div>';
        break;
      case 'accordion_bs_title':
        return '<div class="accordion-header"><button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#' . $id_item . '" aria-expanded="false" aria-controls="' . $id_item . '">' . $content . '</button></div>';
        break;
      case 'accordion_bs_content':
        return '<div id="' . $id_item . '" class="accordion-collapse collapse" data-bs-parent="#' . $id . '"><div class="accordion-body">' . remplacer_tags($content) . '</div></div>';
        break;
        // Alertes
      case 'alert_bs':
        return '<div class="alert alert-dismissible fade show alert-' . $style . '" role="alert">' . remplacer_tags($content) . '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Fermer"></button></div>';
        break;
      case 'alert_bs_title':
        return '<div class="h4 alert-heading">' . $content . '</div>';
        break;
      case 'alert_bs_content':
        return '<p>' . remplacer_tags($content) . '</p>';
        break;
      case 'alert_bs_content_optionnel':
        return '<hr><p class="mb-0">' . remplacer_tags($content) . '</p>';
        break;
        // Badges
      case 'badge_bs':
        return '<span class="badge text-bg-' . $style . '">' . $content . '</span>';
        break;
        // Progress Bars
      case 'progress_bs':
        return '<div class="progress mb-5" role="progressbar" aria-label="Progression" aria-valuenow="' . str_replace('%', '', $content) . '" aria-valuemin="0" aria-valuemax="100" style="height:' . $height . '">' .
          '<div class="progress-bar text-bg-' . $style . '" style="width: ' . $content . '">' . $content . '</div>' .
          '</div>';
        break;
        // Navs
      case 'nav_bs_menu':
        return '<ul class="nav nav-tabs" role="tablist">' . remplacer_tags($content) . '</ul>';
        break;
      case 'nav_bs_title':
        $i++;
        return '<li class="nav-item" role="presentation">' .
          '<button class="nav-link' . (($i == 1) ? ' active' : '') . '" id="' . $id . '_tab_' . $i . '" data-bs-toggle="tab" data-bs-target="#' . $id . '_pane_' . $i . '" type="button" role="tab" aria-controls="' . $id . '_pane_' . $i . '" aria-selected="true">' .
          $content .
          '</button>' .
          '</li>';
        break;
      case 'nav_bs_content':
        return '<div class="tab-content">' . remplacer_tags($content) . '</div>';
        break;
      case 'nav_bs_pane':
        $j++;
        return '<div class="tab-pane border border-top-0 p-3 mb-5 fade' . (($j == 1) ? ' show active' : '') . '" id="' . $id . '_pane_' . $j . '" role="tabpanel" aria-labelledby="' . $id . '_tab_' . $j . '" tabindex="0">' .
          remplacer_tags($content) .
          '</div>';
        break;
      default:
        return $matches[0];
    }
  }, $input);

  return $result;
}
