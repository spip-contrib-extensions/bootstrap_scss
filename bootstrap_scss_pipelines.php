<?php

/**
 * Utilisations de pipelines par Bootstrap SCSS
 *
 * @plugin     Bootstrap SCSS
 * @copyright  2023
 * @author     Pierre-jean CHANCELLIER
 * @licence    GNU/GPL v3
 * @package    SPIP\bootstrap_scss\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
  return;
}

function bootstrap_scss_header_prive($flux) {
  $flux .= recuperer_fond('prive/js/bootstrap_scss.js');
  $flux .= '<script type="text/javascript" src="' . find_in_path("lib/jscolor/jscolor.min.js") . '"></script>';
  $flux .= '<script>jscolor.presets.default = {	format:\'hexa\'};</script>';
  return $flux;
}

function bootstrap_scss_insert_head($flux) {
  $flux .= '<script type="text/javascript" src="/lib/bootstrap532/dist/js/bootstrap.bundle.min.js"></script>';

  if (lire_config('/meta_bootstrap_scss/themes/enable-dark-mode', 'true') == 'true') {
    $flux .= '<script type="text/javascript" src="' . find_in_path("js/theme_bs_toggler.js") . '"></script>';
  }

  return $flux;
}

function bootstrap_scss_insert_head_css($flux) {
  $css = scss_select_css('scss/main.css');
  $flux .= '<link rel="stylesheet" href="' . $css . '" type="text/css" media="all" />';
  return $flux;
}

// Affiche qqe chose dans la colonne de gauche
function bootstrap_scss_affiche_gauche($flux) {
  include_spip('inc/presentation');

  $flux['data'] .=
    debut_cadre_relief('', true, '', 'Page de demo Bootstrap') .
    '<a href="/?page=demo/test_bootstrap_scss" target="_blank">?page=demo/test_bootstrap_scss</a><br><br>' .
    '<p class="explication">Cette page prend en compte votre configuration. Pour un affichage correct des composants, il faut d\'abord <a href="?exec=configurer_composants">les activer</a>.</p>' .
    '<p class="explication danger">⚠ NB : Vous pouvez utilisez des variables SASS dans les paramètres au lieu de mettre du CSS. Mais il faut <strong>ABSOLUMENT</strong> les définir dans le back-office sinon Sass indiquera une erreur.' .
    ' Si malgré tout l\'erreur persiste, c\'est qu\'il faut revoir l\'ordre des boucles dans le fichier <code>/scss/_vars.scss.html</code>. Dans ce cas, créez un ticket <a href="https://git.spip.net/spip-contrib-extensions/bootstrap_scss">sur le dépôt du plugin</a>.</p>' .
    fin_cadre_relief(true);

  return $flux;
}

function bootstrap_scss_porte_plume_barre_pre_charger($barres) {
  $barre = &$barres['edition'];

  $composants_actifs = lire_config('/meta_bootstrap_scss/composants_actifs', []);
  $composants_outils = ['collapse', 'accordion', 'list-group', 'alert', 'badge', 'carousel', 'progress', 'nav'];
  $dropmenu = [];

  foreach ($composants_actifs as $composant) {
    if (in_array($composant, $composants_outils)) {
      $outil = [
        'id' => 'bootstrap_' . $composant,
        'name' => _T('bootstrap_scss:' . $composant),
        'className' => 'bootstrap_' . $composant,
      ];

      switch ($composant) {
        case 'collapse':
          $outil['replaceWith'] = "\n<collapse_bs>\n" .
            "\t<collapse_bs_btn>Title</collapse_bs_btn>\n" .
            "\t<collapse_bs_content>Content</collapse_bs_content>\n" .
            "</collapse_bs>\n";
          break;
        case 'accordion':
          $outil['replaceWith'] = "\n<accordion_bs>\n" .
            "\t<accordion_bs_item>\n" .
            "\t\t<accordion_bs_title>Title</accordion_bs_title>\n" .
            "\t\t<accordion_bs_content>Content</accordion_bs_content>\n" .
            "\t</accordion_bs_item>\n" .
            "\t<accordion_bs_item>\n" .
            "\t\t<accordion_bs_title>Title</accordion_bs_title>\n" .
            "\t\t<accordion_bs_content>Content</accordion_bs_content>\n" .
            "\t</accordion_bs_item>\n" .
            "</accordion_bs>\n";
          break;
        case 'list-group':
          $outil['replaceWith'] = "\n<listgroup_bs>\n" .
            "\t<listgroup_bs_item>Item 1</listgroup_bs_item>\n" .
            "\t<listgroup_bs_item style='primary'>Item 2</listgroup_bs_item>\n" .
            "\t<listgroup_bs_item style='danger'>Item 3</listgroup_bs_item>\n" .
            "</listgroup_bs>\n";
          break;
        case 'alert':
          $outil['replaceWith'] = "\n<alert_bs style='primary'>\n" .
            "\t<alert_bs_title>Title</alert_bs_title>\n" .
            "\t<alert_bs_content>Content</alert_bs_content>\n" .
            "\t<alert_bs_content_optionnel>Optional Content</alert_bs_content_optionnel>\n" .
            "</alert_bs>\n";
          break;
        case 'badge':
          $outil['replaceWith'] = "\n<badge_bs style='primary'>Content</badge_bs>\n";
          break;
        case 'progress':
          $outil['replaceWith'] = "\n<progress_bs style='primary' height='20px'>50%</progress_bs>\n";
          break;
        case 'carousel':
          $outil['replaceWith'] = "\n<carousel_bs>\n" .
            "\t<carousel_bs_item><img src='https://picsum.photos/id/237/400/200'></carousel_bs_item>\n" .
            "\t<carousel_bs_item><img src='https://picsum.photos/id/238/400/200'></carousel_bs_item>\n" .
            "\t<carousel_bs_item><img src='https://picsum.photos/id/239/400/200'></carousel_bs_item>\n" .
            "</carousel_bs>\n";
          break;
        case 'nav':
          $outil['replaceWith'] = "<nav_bs_menu>\n" .
            "\t<nav_bs_title>Titre 1</nav_bs_title>\n" .
            "\t<nav_bs_title>Titre 2</nav_bs_title>\n" .
            "</nav_bs_menu>\n" .
            "<nav_bs_content>\n" .
            "\t<nav_bs_pane>\n" .
            "\t\tPlacez votre texte ici\n" .
            "\t</nav_bs_pane>\n" .
            "\t<nav_bs_pane>\n" .
            "\t\tPlacez votre texte ici\n" .
            "\t</nav_bs_pane>\n" .
            "</nav_bs_content>";
          break;
      }

      $dropmenu[] = $outil;
    }
  }

  if (!empty($dropmenu)) {
    $barre->ajouterApres('grpCode', array(
      "id" => "bootstrap_drop",
      "name" => "Bootstrap",
      "className" => "bootstrap_drop",
      "display"   => true,
      "dropMenu" => $dropmenu
    ));
  }

  return $barres;
}

function bootstrap_scss_porte_plume_lien_classe_vers_icone($flux) {
  $icons = [
    'bootstrap_drop' => 'bootstrap.png',
    'bootstrap_collapse' => 'bs_collapse.png'
  ];;

  if ($composants_actifs = lire_config('/meta_bootstrap_scss/composants_actifs')) {
    foreach ($composants_actifs as $composant) {
      $icons['bootstrap_' . $composant] = 'bs_' . $composant . '.png';
    }
  }

  return array_merge($flux, $icons);
}
